---
layout: markdown_page
title: "UX Team"
---

## UX Workflow 

### Designer

Issues should be tagged with 'UX' if UX work is required.

1. Work on issues tagged with 'UX' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux) and [EE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux). Prioritize issues scheduled for the current milestone. If all the issues for the current milestone have been addressed, seek out issues tagged 'coming soon'.
1. Once UX work is completed and feedback addressed, unassign yourself and remove the UX label. Add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) needed for the issue. Typically, this is the Frontend label.
1. Continue to follow the issue, addressing any additional UX issues that come up.

### Researcher

Issues should be tagged with 'UX Research' if there may be an opportunity for research.

1. Work with the UX team to determine the question to research.
2. Create an issue to track the work. The issue should include the question to be researched, the hypothesis, what the impact will be based on how the question is answered, and what research method(s) will be used. List related issues.
3. Tag the issue with 'UX Research' and assign it to yourself.
4. Conduct the research and document the findings and recommendations in the issue.
5. Assign the issue to the UX Designer who is going to implement the design based on the research. Remove the 'UX Research' tag and add the 'UX' tag.

*Also see the [basics of GitLab development in the developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/#basics-of-gitlab-development).*