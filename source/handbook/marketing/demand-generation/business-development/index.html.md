---
layout: markdown_page
title: "Business Development"
---
Business Development Handbook

## On this page
* [Your Role](#Role)
* [Outbound BDR Onboarding Guide](#BDRO)
* [Week 1](#Week1)  
* [Week 2](#Week2)
* [Week 3](#Week3) 
* [Outbound Process](#Outbound)
* [Inbound BDR Onboarding Guide](#BDROn)
* [Week 1](#Week1in)  
* [Week 2](#Week2in)
* [Week 3](#Week3in)
* [Inbound Process](#Inbound)
* [Variable Compensation Guidelines](#compensation)
* [Vacation Days](#vacation)
* [Prospecting 101](#Basic)
* [How to Cold Call](#Cold)
* [How to Email](#Email)

#### Welcome to the team!<a name="Role"></a>
Welcome to GitLab and congratulations on landing a job with the best open source tech company, GitLab. We are excited to have you join the team! We look forward to working closely with you and seeing you grow and develop into a top performing salesperson. 

On the BDR team we work hard, but have fun too. We will work hard to garuntee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR can come with what seems as long days, hard work, frustration and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen!

#### Your Role
As a Business Development Representative, you will be dealing with the front end of the sales process. You play a crucial role that helps bridge the gap between sales and marketing. You will be tasked with generating qualified opportunities for GitLab as you look for leads, research companies, industries and different roles.

As you gain knowledge, you will be able to aid these key players in solving problems within the developer lifecycle. There are numerous resources at your fingertips that we have created to help you in this process. You have:

1: [Onboarding Issue](https://dev.gitlab.org) - This will typically fill up the majority of your first week. This is a step by step guide and checklist to getting everything in your arsenal set up like equipment, gmail, calendly, slack, security, and your Gitlab.com account. These todo’s provide you with the fundamentals. 

2: [BDR Handbook](https://about.gitlab.com/handbook/marketing/demand-generation/business-development/) - This will help you with your day to day workflow. You can find information on how to prospect, best practices, customer FAQs, buyer types, cadence samples and more. Use the BDR handbook in conjunction with the [marketing](https://about.gitlab.com/handbook/marketing/) and [sales](https://about.gitlab.com/handbook/sales/) handbooks. This will help you bridge the gap between the two and learn the product and process faster.

3: [GLU GitLab University](https://university.gitlab.com/) - These trainings will teach the fundamentals of Version Control with Git and GitLab through courses that cover topics which can be mastered in about 2 hours. These trainings include an intro to Git, GitLab basics, a demo of Gitlab.com, terminology and more. You can also find information about GitLab compared to other tools in the market.

The rest of this guide will outline your onboarding at GitLab. Do not take this to be concrete. We hire very talented individuals who take initiative and advantage of the opportunities and situations presented to them. Be creative, learn and try different ways of doing things. We are excited to have you and cannot wait to see what you bring to the team!

## Outbound BDR Onboarding Guide<a name="BDRO"></a> 

#### Frequently Asked Questions
I’m having issues with Salesforce - can’t login, my leads are not syncing, salesforce isn't working, I want to request more contacts from discoverorg, not sure what the protocol is, etc.
* Slack - Francis Aquino

My GitLab account isn’t working. I can’t seem to login or access anything
* Slack - Assigned Buddy

What are my work hours?
* The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”

How do I know who to assign a lead to?

* [Here](https://docs.google.com/document/d/1Zh63p4PL5LCyneW_Jpewv-gGXTkAleaklSJ7z3spGLw/edit?ts=57e98e6b)

How do I create an opportunity?

* [Here](https://docs.google.com/document/d/1Txt9_ErefZpoPMSm6tCp5tZhO6am7qv3fXzrZo84K3k/edit)

How do I access the handbook?

* [Here](https://about.gitlab.com/primer/)

How do I BDR APAC and SA contacts

* [Here](https://docs.google.com/a/gitlab.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit?usp=sharing)

* Note: BDRs do not set up meetings for resellers.

#### What links should I be familiar with?
* [Trinet](https://sso.trinet.com/)

* [Salesforce](https://login.salesforce.com/)

* [Slack](https://gitlab.slack.com)

* [Expensify](https://www.expensify.com)

* [Bamboohr](https://gitlab.bamboohr.com)

* [Calendly](https://calendly.com/)

* [Collabspot](https://go.collabspot.com)

* [GitLab Version Check](https://version.gitlab.com)

#### What Slack channels should I join?
 #bdr-team
 
 #general
 
 #marketing
 
 #peopleops
 
 #questions
 
 #random
 
 #sales
 
 #sfdc-users
 
 #support
 
 #thanks
 
 #wins
 
 #working-on
 
#### Who should I connect with for my 10 1:1 onboarding calls?
* You can connect with whomever you please. However, you will interact the most with the Sales Team and the Demand Generation Team 

    * [GitLab Team Page](https://about.gitlab.com/team/)

#### Coined Terms
 #BDRing - Doing your job...Kris Touzel
 
 #SQLing - Winning at your job...Ryan Caplin
 
 #BDR - Boss Destroying Revenue...Joe Lucas
 
#### Week One<a name="Week1"></a>  

###### Training
* [Intro to Git](https://www.codeschool.com/users/sign_in)

* Watch the [Vision Demo](https://about.gitlab.com/handbook/sales/vision-demo/): Idea to Production 

* Read [GitLab Direction](https://about.gitlab.com/direction/)

* Study GLU: [GitLab University](https://university.gitlab.com/)

* Read the [Hanbook](https://about.gitlab.com/handbook/)

###### To Do List
* [Onboarding Issue](https://dev.gitlab.org)

* Create an email signature [Sample](https://docs.google.com/document/d/1NvfC4a26G0WZwxB1K2oeXyGvI0DGqy-LbTKa9iPP6kU/edit)

* Open Salesforce welcome email and activate account

* Open Collabspot welcome email and activate account

* Schedule time with your assigned mentor

###### Metrics
* Complete your onboarding issue

###### By the end of your first week you should (know) and be able to do the following:
* What is GitLab (from a high level)

* Create and assign merge requests (from your onboarding issue)

* Sign in to SFDC (Salesforce.com)

* Sign in to Linkedin

#### Week Two<a name="Week2"></a>

###### Training
* [GitLab University](https://university.gitlab.com/)

* SFDC Training TBD

* InsideView Overview TBD

* [Sales Process](https://about.gitlab.com/handbook/sales/) and [Qualification](https://docs.google.com/document/d/1GR0v4cvBYUoTSn66kVAqzY5q_imWbDWW4ifo7ag6Gow/edit#heading=h.ebl8fraqpynv)
 
* Prospecting 101 by Scheduled by Chet

###### To Do List
* Mine 5 leads each day using Linkedin Sales Navigator and Inside View then add them as contacts to SFDC

* Create tasks for each contact

* Create a task list in SFDC of at least 25 contacts/leads

* Shadow 3 Outbound BDRs for one hour

 * B.J. (Oct 17)

 * Elsje Smart

 * D.E. (Oct 17)

###### Metrics
* 50 Prospecting Calls####

* 25 Prospecting Emails

* 25 New contacts added to your task list

* First SQL

###### By the end of your second week you should (know) and be able to do the following:
* Create and add contacts to SFDC

* How to manage your task list in SFDC

* How to use InsideView

* Craft a basic prospecting email

* Be assigned a list of accounts to work by your Team Lead

#### Week Three<a name="Week3"></a>

###### Sales Training
* [GitLab University](https://university.gitlab.com/)

* Manager Session

* BDR Ongoing Training (Friday)

###### To Do
* Schedule 30 minutes with the AE’s that own your accounts (Accounts will assigned to you by your team lead) to  collaborate, strategize, and plan.

* Have a healthy task list of at least 30 contacts/leads to target

* Ask AEs to sit in on three discovery calls
  
    * Josh Wrede
    
    * Phil Camillo

    * Mike Walsh
  
###### Metrics
* Generate three SQLs

* 100 Prospecting Calls

* 50 Prospecting Emails

* 25 New contacts added to your task list

    * Note - We do not have minimum call/email metrics that you are held against. Be tenacious in your efforts. Remember the more at bats you have the better chance you have of hitting a home run. The numbers reflected above are best practices to get you ramped and ready for your everyday workflow.

###### By the end of your third week you should (know) and be able to do the following:
* Create and add contacts to SFDC

* How to manage your task list in SFDC

* How to use InsideView

* How to BDR

## Outbound Process<a name="Outbound"></a>

Each rep will be assigned a list of 15-30 targeted accounts to work on a monthly or quarterly basis.

#### Criteria for those accounts still TBD
- Tier
- Industry
- Number of Employees
- Revenue
- Populated with contacts and activity
- Last activity date
- Lost opportunities
- Former or current customers

#### Help Marketing create campaigns focusing on:
- Accounts using specific competitors
- Up-to-date Titles
- Up-to-date company sizes
- Contract end dates

#### Rules of engagement
- Outbound to work off of contacts and leads within SFDC
- Outbound does not touch any historical lead that has activity on it within the last 45 days. We are on the same team as inbound, no bad blood, etc.

#### Formal Weekly 1:1

BDR and BDR Team Lead

- Mental check-in (winning and success)

- Discuss progress on targeted accounts

- Coaching - email strategy, campaigns, cadence, best practices

- Review goals at the account level and personal level

- Strategy for next week

- Upcoming events/campaigns that can be leveraged

- Personal goals and commitments 

## Inbound BDR Onboarding Guide<a name="BDROn"></a>

#### Frequently Asked Questions
I’m having issues with Salesforce - can’t login, my leads are not syncing, salesforce isn't working, I want to request more contacts from discoverorg, not sure what the protocol is, etc.
* Slack - Francis Aquino

My GitLab account isn’t working. I can’t seem to login or access anything
* Slack - Assigned Buddy

What are my work hours?
* The handbook says “You should have clear objectives and the freedom to work on them as you see fit. Any instructions are open to discussion. You don’t have to defend how you spend your day. We trust team members to do the right thing instead of having rigid rules”

How do I know who to assign a lead to?

* [Here](https://docs.google.com/document/d/1Zh63p4PL5LCyneW_Jpewv-gGXTkAleaklSJ7z3spGLw/edit?ts=57e98e6b)

How do I convert a lead to an opportunity?

* [Here](https://docs.google.com/document/d/1Txt9_ErefZpoPMSm6tCp5tZhO6am7qv3fXzrZo84K3k/edit)

How do I access the handbook?

* [Here](https://about.gitlab.com/primer/)

How do I deal with APAC and SA leads?

* [Here](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit)

* *Note: BDRs do not set up meetings for resellers.

#### What links should I be familiar with?
* [Trinet](https://sso.trinet.com/)

* [Salesforce](https://login.salesforce.com/)

* [Slack](https://gitlab.slack.com)

* [Expensify](https://www.expensify.com)

* [Bamboohr](https://gitlab.bamboohr.com)

* [Calendly](https://calendly.com/)

* [Collabspot](https://go.collabspot.com)

* [GitLab Version Check](https://version.gitlab.com)

#### What Slack channels should I join?
 #bdr-team
 
 #general
 
 #marketing
 
 #peopleops
 
 #questions
 
 #random
 
 #sales
 
 #sfdc-users
 
 #support
 
 #thanks
 
 #wins
 
 #working-on
 
#### Who should I connect with for my 10 1:1 onboarding calls?
* You can connect with whomever you please. However, you will interact the most with the Sales Team and the Demand Generation Team 

    * [GitLab Team Page](https://about.gitlab.com/team/)

#### Coined Terms
 #BDRing - Doing your job...Kris Touzel
 
 #SQLing - Winning at your job...Ryan Caplin
 
 #BDR - Boss Destroying Revenue...Joe Lucas
 
#### Week One<a name="Week1in"></a>  

###### Training
* [Intro to Git](https://www.codeschool.com/users/sign_in)

* Watch the [Vision Demo](https://about.gitlab.com/handbook/sales/vision-demo/): Idea to Production 

* Read [GitLab Direction](https://about.gitlab.com/direction/)

* Study GLU: [GitLab University](https://university.gitlab.com/)

* Read the [Hanbook](https://about.gitlab.com/handbook/)

###### To Do List
* [Onboarding Issue](https://dev.gitlab.org)

* Create an email signature [Sample](https://docs.google.com/document/d/1NvfC4a26G0WZwxB1K2oeXyGvI0DGqy-LbTKa9iPP6kU/edit)

* Open Salesforce welcome email and activate account

* Open Collabspot welcome email and activate account

* Schedule time with your assigned mentor

###### Metrics
* Complete your onboarding issue

###### By the end of your first week you should (know) and be able to do the following:
* What is GitLab (from a high level)

* Create and assign merge requests (from your onboarding issue)

* Sign in to SFDC (Salesforce.com)

* Sign in to Linkedin

#### Week Two<a name="Week2in"></a>

###### Training
* [GitLab University](https://university.gitlab.com/)

* SFDC Training TBD

* InsideView Overview TBD

* [Sales Process](https://about.gitlab.com/handbook/sales/) and [Qualification](https://docs.google.com/document/d/1GR0v4cvBYUoTSn66kVAqzY5q_imWbDWW4ifo7ag6Gow/edit#heading=h.ebl8fraqpynv)
 
* Prospecting 101 by Scheduled by Chet

    * **Note**: You will recieve 200 inbound leads to start working this week

###### To Do List
* Add these suggested [canned responses](https://docs.google.com/a/gitlab.com/document/d/1EektuIAJKo9fBe-EiAnPR3BHhlkdaWE4wqG2z3QuP5o/edit?usp=sharing) to your gmail for quick replies 

* Create tasks for the leads you choose to work

* Create a task list in SFDC of at least 15 leads

* Shadow 3 Inbound BDRs for one hour

    * See [Team Page](https://about.gitlab.com/team/)

###### Metrics
* First SQL

* 25 Prospecting Calls

* 25 Prospecting Emails

* Maintain a task list of at least 15 leads

###### By the end of your second week you should (know) and be able to do the following:
* Create tasks for leads within SFDC

* How to manage your task list in SFDC

* How to use InsideView

* Craft a basic response email

#### Week Three<a name="Week3in"></a>

###### Training
* [GitLab University](https://university.gitlab.com/)

* Manager Session

* BDR Ongoing Training (Friday)

###### To Do List
* You will start recieving live leads 

* Schedule time with each of the AE's to collaborate and learn about lead management **Note** There is no agenda for this meeting, attend prepared with questions 

* Have a healthy task list of at least 25 leads to target

* Sit in on three discovery calls with the AE's

    * [Team Page](https://about.gitlab.com/team/)
  
###### Metrics
* Generate three SQLs

* 50 Prospecting Emails

    * **Note** - We do not have minimum call/email metrics that you are held against. Be tenacious in your efforts. Remember the more at bats you have the better chance you have of hitting a home run. The numbers reflected above are best practices to get you ramped and ready for your everyday workflow.

###### By the end of your third week you should (know) and be able to do the following:
* Convert leads into opportunities

* How to manage your task list in SFDC

* How to use InsideView

* How to BDR

## Inbound Process<a name="Inbound"></a>

Each rep will be placed into the Marketo Queue and will recieve a high volume of leads to work on a monthly and quarterly basis

Criteria for those leads are set by Marketo and the Director of Demand Generation

#### Primary Responsibilities:
* Manage and help inbound requests to community@gitlab.com and sales@gitlab.com

* Strategize to develop the proper qualifying questions for all types of customers.

* Be able to identify where a customer is in the sale and marketing funnel and take the appropriate action

* Generate Sales Qualified Leads (SQLs)

#### Rules of engagement
- Inbound to work off of leads within SFDC
- Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR. 

#### BDR and BDR Team Lead

- Mental check-in (winning and success)

- Discuss progress on targeted accounts

- Coaching - email strategy, campaigns, cadence, best practices

- Review goals at the account level and personal level

- Strategy for next week

- Upcoming events/campaigns that can be leveraged

- Personal goals and commitments 

#### Variable Compensation Guidelines<a name="compensation"></a>

Full-time BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on results. Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

Guidelines for bonuses:
* Team and individual objectives are based on GitLab's revenue targets and adjusted monthly.
* Bonuses are paid quarterly.
* Bonuses may be based on various objectives
    * E.g. a new BDR's first month's bonus is typically based on completing <a href="https://about.gitlab.com/handbook/general-onboarding/" target="_blank">onboarding</a>
    * Bonuses may be tied to sales target attainment, <a href="https://about.gitlab.com/handbook/marketing/#okrs" target="_blank">OKRs</a>, or other objectives.

#### BDR Vacation Incentives<a name="vacation"></a>

Note: this is a new, experimental policy started in June 2016: changes and adjustments are expected

At GitLab, we have an <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">unlimited time off policy</a>. The business development team has additional incentives for taking time off:
1. The first 5 business days taken off by a BDR in a calendar year prorates their target by 100%, rounded up (i.e. reduces the BDR's target by the period's target divided by the number of business days in the period, times the number of days taken off).
2. The second 5 business days are prorated by 50%, rounded up if applicable
3. The third 5 business days are prorated by 25%, rounded up if applicable

E.g. A BDR with a target of 200 opportunities in a January with 20 business days can reduce that month's target to 190 by taking 1 day off, 150 by taking 5 days off, 125 by taking 10 days off, or 113 by taking 15 days off.

Rules and qualifications for BDR Vacation Incentives:

* BDRs must give notice 30 days ahead of intended time off to by the Team Lead and Senior Demand Generation Manager (and ensure they acknowledge receipt within 48 hours)
* BDRs must add intended time off to the availability calendar
* Prorated days can be deferred (i.e. take PTO without prorating the target), but are lost at the end of the year. Incidentally, the math usually works out in favor of not deferring.
* BDRs can take additional vacation days (see <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab PTO policy</a>), but time off beyond 15 days will not adjust targets.

With <a href="https://about.gitlab.com/handbook/#paid-time-off" target="_blank">GitLab's PTO policy</a>), there's no need to give notice or ask permission to take time off. Unfortunately, a quota carrying BDR can expect to lose some earning potential for taking PTO, which often leads to BDRs not taking vacation. Another problem with unlimited PTO is that management loses forecasting power when BDRs take unexpected PTO, making it tempting for leadership to discourage vacation. We hope that this additional vacation incentive for the team acts as a balancing incentive to both parties by (a) providing a prorated quota for BDRs who plan ahead and (b) helping leadership plan and forecast better without discouraging vacation.

#### Additional Resources
- <a href="https://university.gitlab.com/" target="_blank">GitLab University</a>
- <a href="https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6" target="_blank">GitLab Market and Ecosystem</a>
- <a href="http://docs.gitlab.com/ee/" target="_blank">GitLab Documentation</a>
- <a href="https://about.gitlab.com/2016/07/22/building-our-web-app-on-gitlab-ci/" target="_blank">GitLab CI</a>
- <a href="https://about.gitlab.com/features/#compare" target="_blank">GitLab Editions</a>
- <a href="https://about.gitlab.com/comparison/" target="_blank">GitLab compared to other tools</a>

## Buyer Types<a name="Buyer Types"></a>

#### Technical Buyer
- (End User)
- Collaborate, features, data

#### Functional Buyer
- (Director)
- Making team projects more collaborative 

#### Economic Buyer
- (C-Level, VP)
- Hit Team Targets and ROI

Your messaging for each one will be different. An end user or lower level developer will not care much about the ROI on our product.

## Research Process<a name="Research Process"></a>

3 X 3 X 3

Three things in three minutes about the:
- Organization
- Industry
- Prospect/Lead

#### Where to Start?

1. Salesforce
    - [Report](https://na34.salesforce.com/00O61000003JOUq) of prospects and customers with 500+ seat potential.
       - Check with Account Owner on how they would like you to proceed with the account.
       - If a customer, also check [Customer Success Issue Tracker](https://gitlab.com/gitlab-com/customer-success/issues) to see if an account expansion plan exists. If one does, please collaborate and contribute in the issue.
    - [Lost deals](https://na34.salesforce.com/00O610000017SDD), users and customers (CE and EE)
    - Contacts
        - What relevant titles should we be reaching out to
    - Activity History
        - Who in the org have we called before that we can follow up with or reference
    - Cross reference with [version.gitlab.com](https://version.gitlab.com/users/sign_in) to see their version and available other usage info

2. Linkedin
    - Summary/Bio - Anything mentioned about job responsibilities or skills
    - Previous work - Any Gitlab Customers that we can reference
    - Connections - Are they connected to anyone at GitLab for a possible introduction

3. Website
    - Mission Statement
    - Press Releases/Newsroom
    - (ctrl F) search for keywords

## Basics of Prospecting<a name="Basic"></a>
- Call about them, not you
- Be confident and passionate
- Aim for every role but focus on decision makers
- Prospect into multiple departments
- Ask for time
- Focus on the your end game
- Make it easy to say yes
- Obtain a commitment

## Cold Call Framework<a name="Cold"></a>

#### Introduction
Hi. This is (Name) from GitLab

#### Ask for Time
“Do you have just a moment?”

#### Question
“Have you heard of GitLab before?”

#### Initial Message with IBS
“GitLab helps (Title) in the (Industry) industry solve issues around (pain & need) or
“(Industry specific client) used GitLab to (Case study value proposition)

#### Confirm that they are the right person
“I understand you are in charge of application/program development, is that correct?

#### Schedule meeting
"Do you have 15 minutes on (specific day and time) to discuss your (example - developer life cycle)?"

#### Initial Benefit Statement example
- GitLab is an end-to-end developer lifecycle solution that allows developers to collaborate on code, project management, and to create a seamless workflow between teams across the entire org.

#### Structure of a Prospecting Call
- Introduction
- Initial Benefit Statement
- Qualification... See the criteria above in the handbook
- Reason for taking the call (add value)
- Inform them about GitLab
- Commitments

## Email Prospecting<a name="Email"></a>

#### Emails
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

#### Email Components
- Subject line
- Preview Pain
- Opening Stanza (Best Practice is something about them)
- Benefit & Value Proposition
- Create Urgency
- Definite Ask with available days and times
