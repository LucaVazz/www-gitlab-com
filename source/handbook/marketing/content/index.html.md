---
layout: markdown_page
title: "Content Team"
---

The Content Team is part of Marketing and responsible for improving the GitLab
[Documentation](https://docs.gitlab.com), [Blog](/blog/) and [Handbook](/handbook/).

## Documentation Goals

* Create a complete learning experience taking users from beginner to expert in Git and GitLab.
* Always update the documentation when tools are released or updated.
* Include GitLab training videos directly in relevant documentation articles.
  Right now all videos are linked to from [GitLab University](https://docs.gitlab.com/ce/university/).
* Use illustrations and animations to explain technical concepts.
* Allow users to comment directly on documentation articles.
* Create a high quality responsive design.
* Allow users to easily browse and search the documentation.
* Generate MQLs

## Blog Goals

Detailed blog [handbook](/handbook/marketing/blog).

* Create high quality and engaging content that shares GitLab's culture and user success stories.
  GitLab's culture:
  * Remote only
  * Innersourcing
  * Open core
  * Conversational Development
  * Idea to Production
  * Cycle Analytics
  * Everyone deserves one boss
  * Small and frequent iterations
  * Allow users to easily browse and search the blog.
* Provide article formulas (recipes) for the team and community to use.
* Use illustrations and animations to enhance the content.
* Generate MQLs.

## Handbook Goals

* Improve the GitLab handbook structure and quality
* The answer to any organizational questions should be "It's in the handbook".
* Share GitLab's culture.
* Generate MQLs.
