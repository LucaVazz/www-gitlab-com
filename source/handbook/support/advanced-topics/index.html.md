---
layout: markdown_page
title: Adcanced Support Topics
---

## Earn the Expert badge by completing any of these

If there is no link to a checklist, you will need to create and document the
learning path as you go. The links to documentation are just to help you get
started with that process. Take a look at the [Geo Checklist](/handbook/support/advanced-topics/geo)
for how to get started and put a **(WIP)** after your link to let people know
they won't get the expert badge by finishing the list in its current state.

Only work on one subject at a time. Inform your manager which one you will be
working on, so that they can let you know if there is a different area where
the team really needs expertise. Always try to answer tickets on other advanced
topics, but when it is time to do some dedicated learning, focus on one area.

### Fixing Bugs on GitLab

### Git Annex

[Git Annex Docs](https://docs.gitlab.com/ee/workflow/git_annex.html)

### Git LFS

[Git LFS Docs](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)

### GitLab API

[GitLab API Docs](https://docs.gitlab.com/ee/api/README.html)

### GitLab CI

[GitLab CI Docs](https://docs.gitlab.com/ee/ci/quick_start/README.html)

### GitLab Geo

[Geo Checklist](/handbook/support/advanced-topics/geo) (WIP)

### GitLab Pages

[GitLab Pages Docs](https://docs.gitlab.com/ee/pages/administration.html)

### Migrating from SVN to Git

[migrate from SVN to Git Docs](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)
